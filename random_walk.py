from random import choice
import matplotlib.pyplot as plt

class RandomWalk:
    """ A class that generates random walks. """

    def __init__(self, num_points=50_000):
        self.num_points = num_points
        self.x = [0]
        self.y = [0]

    def set_steps(self):
        """ Helper method to calculate the steps """

        temp_dir = choice([-1, 1])
        temp_dist = choice([0, 1, 2, 3, 4, 5])
        return temp_dir * temp_dist

    def calc_walk(self):
        """ Calculate all points in a walk. """

        # Walking as long as desired length.
        while len(self.x) < self.num_points:
            
            # Which direction to go and how far
            x_step = self.set_steps()
            y_step = self.set_steps()

            # Ignore useless moves
            if x_step == 0 and y_step == 0:
                continue
            
            # Calculate new position
            x_new = self.x[-1] + x_step
            y_new = self.y[-1] + y_step

            self.x.append(x_new)
            self.y.append(y_new)


while (True):

    random_walk = RandomWalk()
    random_walk.calc_walk()

    # Plotting
    plt.style.use('classic')
    fig, ax = plt.subplots()
    number_of_points = range(random_walk.num_points)
    ax.scatter(random_walk.x, random_walk.y, c=number_of_points, cmap=plt.cm.Blues, edgecolor='none', s=3)
    
    # Clarify start/end points
    ax.scatter(0, 0, c='green', edgecolors='none', s=50)
    ax.scatter(random_walk.x[-1], random_walk.y[-1], c='red', edgecolors='none', s=50)
    
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    plt.show()

    another_walk = input("Another random walk? (y/n): ")
    if another_walk == 'n':
        break