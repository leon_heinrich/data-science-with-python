from random import randint 

class Die:
    """ Represent a die. """

    def __init__(self, num_of_sides=6):
        """ Initialize the die with the required number of sides. """

        self.num_of_sides = num_of_sides

    def roll_die(self):
        """ Represents rolling the die by returning a random value. """

        return randint(1, self.num_of_sides)