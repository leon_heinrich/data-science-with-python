from plotly.graph_objs import Bar, Layout
from plotly import offline

from die import Die

die = Die()

results = []
for num_of_rolls in range(1_000):
    result = die.roll_die()
    results.append(result)

# Analysis
occurances = []
for value in range(1, die.num_of_sides+1):
    occurance = results.count(value)
    occurances.append(occurance)

# Visualization
x_val = list(range(1, die.num_of_sides+1))
data = [Bar(x=x_val, y=occurances)]

x_config = {'title': 'Side of Die'}
y_config = {'title': 'Number of occurances'}
chart_layout = Layout(title='Rolling a 6-sided die 1000 times - Results', xaxis=x_config, yaxis=y_config)
offline.plot({'data': data, 'layout': chart_layout}, filename='./rolling_dice/res_d6.html')