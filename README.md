# Data Science with Python

## Highlights

### Data Visualization

1) Plotting with Matplotlib/Pyplot and the help of numpy and the math functions sin and cos:

<img src="/images/my_plot.png"  width="500"><br/><br/>


2) Also used an approach to print random steps during a walk:

<img src="/images/random_walk1.png"  width="300">
<img src="/images/random_walk2.png"  width="300"><br/><br/>


3) Bar chart for the results of 1000 throws of a 6-sided die:

<img src="/images/die_bar_chart.png"  width="700">

