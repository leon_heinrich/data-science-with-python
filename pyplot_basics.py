import matplotlib.pyplot as plt

values = [2, 3, 4, 5, 6] # default input would be [0,1,2,3,4] ^= wrong
square_numbers = [4, 9, 16, 25, 36]

plt.style.use("seaborn") # styling before the plot initialization

figures, sp = plt.subplots()
sp.plot(values, square_numbers, linewidth=2)

# Styling the plot 
sp.set_title("Square Numbers", fontsize=20)
sp.set_xlabel("Value", fontsize=15)
sp.set_ylabel("Square of Value", fontsize=15)

# sp.tick_params(axis="both", fontsize=11)

plt.show()