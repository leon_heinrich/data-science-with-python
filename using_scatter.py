import matplotlib.pyplot as plt
import math
import numpy as np

input_val = np.arange(-math.pi*3, math.pi*3, 0.01)

sinus_plot = [math.sin(val) for val in input_val]
cosinus_plot = [math.cos(val) for val in input_val]

plt.style.use("seaborn") # styling before the plot initialization

figures, sp = plt.subplots()
sp.scatter(input_val, sinus_plot, c=sinus_plot, cmap=plt.cm.Reds, s=10)
sp.scatter(input_val, cosinus_plot, c=cosinus_plot, cmap=plt.cm.Blues, s=10)

# Styling the plot 
sp.set_title("Fading Trigonometry", fontsize=20)
sp.set_xlabel("x", fontsize=15)
sp.set_ylabel("y", fontsize=15)

sp.ticklabel_format(style="plain")

# Range setting 
sp.axis([-10, 10, -1.5, 1.5])

# plt.show()
plt.savefig("./data_visualization/my_plot.png")